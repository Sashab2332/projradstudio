//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void Tfm::SelectionAll(TObject *Sender)
{
if (FSel!=NULL) {
		  FSel->HideSelection=true;
}
FSel=dynamic_cast<TSelection*>(Sender);
if (FSel!=NULL) {
  FSel->HideSelection=false;
}

//
tbOptions->Visible=(FSel!=NULL);
if (tbOptions->Visible) {
	trRotation->Value=FSel->RotationAngle;
}
//
tbImage->Visible=(FSel!=NULL)&&(dynamic_cast<TGlyph*>(FSel->Controls->Items[0]));
//
tbRect->Visible=(FSel!=NULL)&&(dynamic_cast<TRectangle*>(FSel->Controls->Items[0]));
if (tbOptions->Visible) {
   ComboColorBoxRect->Color=((TRectangle*)FSel->Controls->Items[0])->Fill->Color;
   trRectRadius->Value=((TRectangle*)FSel->Controls->Items[0])->XRadius;
}
}
void __fastcall Tfm::Selection1MouseDown(TObject *Sender, TMouseButton Button,
		  TShiftState Shift, float X, float Y)
{
SelectionAll(Sender);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
FSel=NULL;
SelectionAll(ly);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBringToFrontClick(TObject *Sender)
{
FSel->BringToFront();
FSel->Repaint();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buSendToBackClick(TObject *Sender)
{
FSel->SendToBack();
FSel->Repaint();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::trRotationChange(TObject *Sender)
{
FSel->RotationAngle=trRotation->Value;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buDelClick(TObject *Sender)
{
FSel->DisposeOf();
FSel=NULL;
SelectionAll(ly);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buImagePrevClick(TObject *Sender)
{
TGlyph *x=(TGlyph*)FSel->Controls->Items[0];
//x->ImageIndex=((int)x->ImageIndex>=dm->il->Count -1)?
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buImageRNDClick(TObject *Sender)
{
TGlyph *x=(TGlyph*)FSel->Controls->Items[0];
x->Fill->Color=ComboColorBoxRect
}
//---------------------------------------------------------------------------
