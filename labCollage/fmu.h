//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Colors.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buNewImage;
	TButton *buNewRect;
	TButton *buClear;
	TButton *buAbout;
	TGlyph *Glyph1;
	TSelection *Selection1;
	TLayout *ly;
	TSelection *Selection2;
	TGlyph *Glyph2;
	TSelection *Selection3;
	TRectangle *Rectangle1;
	TToolBar *tbOptions;
	TButton *buBringToFront;
	TButton *buSendToBack;
	TButton *buDel;
	TTrackBar *trRotation;
	TToolBar *tbImage;
	TButton *buImagePrev;
	TButton *buImageNext;
	TButton *buImageRND;
	TButton *buImageSelect;
	TToolBar *tbRect;
	TComboColorBox *ComboColorBoxRect;
	TTrackBar *trRectRadius;
	void __fastcall Selection1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buBringToFrontClick(TObject *Sender);
	void __fastcall buSendToBackClick(TObject *Sender);
	void __fastcall trRotationChange(TObject *Sender);
	void __fastcall buDelClick(TObject *Sender);
	void __fastcall buImagePrevClick(TObject *Sender);
	void __fastcall buImageRNDClick(TObject *Sender);
private:	// User declarations
TSelection *FSel;
void SelectionAll(TObject *Sender);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
