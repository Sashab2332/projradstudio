//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Ttm *tm;
//---------------------------------------------------------------------------
__fastcall Ttm::Ttm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Ttm::Panel1Click(TObject *Sender)
{
//Panel1->Color = clRed;
}
//---------------------------------------------------------------------------
void __fastcall Ttm::FormCreate(TObject *Sender)
{
tc->ActiveTab= timain;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::buNewClick(TObject *Sender)
{
 tc->GotoVisibleTab(tilevel0->Index);
}
//---------------------------------------------------------------------------

void __fastcall Ttm::buLevel1Click(TObject *Sender)
{
 tc->GotoVisibleTab(tilevel1->Index);
}
//---------------------------------------------------------------------------


void __fastcall Ttm::Rectangle82Click(TObject *Sender)
{
coOrange->Tag=0;
coBrown->Tag=0;
coYellow->Tag=0;
coPink->Tag=0;
coBlack->Tag=1;
coOrange->Enabled=true;
coBrown->Enabled=true;
coYellow->Enabled=true;
coPink->Enabled=true;
coBlack->Enabled=false;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle83Click(TObject *Sender)
{
coBlack->Tag=0;
coBrown->Tag=0;
coYellow->Tag=0;
coPink->Tag=0;
coOrange->Tag=1;
coBlack->Enabled=true;
coBrown->Enabled=true;
coYellow->Enabled=true;
coPink->Enabled=true;
coOrange->Enabled=false;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle84Click(TObject *Sender)
{
coOrange->Tag=0;
coBlack->Tag=0;
coYellow->Tag=0;
coPink->Tag=0;
coBrown->Tag=1;
coOrange->Enabled=true;
coBlack->Enabled=true;
coYellow->Enabled=true;
coPink->Enabled=true;
coBrown->Enabled=false;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle85Click(TObject *Sender)
{
coBlack->Tag=0;
coBrown->Tag=0;
coOrange->Tag=0;
coPink->Tag=0;
coYellow->Tag=1;
coOrange->Enabled=true;
coBrown->Enabled=true;
coBlack->Enabled=true;
coPink->Enabled=true;
coYellow->Enabled=false;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle86Click(TObject *Sender)
{
coBlack->Tag=0;
coBrown->Tag=0;
coOrange->Tag=0;
coYellow->Tag=0;
coPink->Tag=1;
coOrange->Enabled=true;
coBrown->Enabled=true;
coBlack->Enabled=true;
coYellow->Enabled=true;
coPink->Enabled=false;
}
//---------------------------------------------------------------------------



void __fastcall Ttm::Rectangle1Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph1->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph1->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph1->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph1->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph1->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle10Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph10->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph10->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph10->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph10->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph10->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle11Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph11->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph11->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph11->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph11->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph11->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle12Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph12->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph12->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph12->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph12->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph12->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle13Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph13->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph13->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph13->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph13->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph13->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle14Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph14->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph14->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph14->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph14->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph14->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle15Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph15->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph15->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph15->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph15->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph15->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle16Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph16->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph16->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph16->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph16->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph16->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle17Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph17->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph17->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph17->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph17->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph17->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle18Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph18->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph18->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph18->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph18->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph18->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle19Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph19->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph19->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph19->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph19->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph19->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle2Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph2->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph2->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph2->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph2->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph2->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle20Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph20->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph20->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph20->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph20->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph20->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle21Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph21->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph21->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph21->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph21->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph21->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle22Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph22->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph22->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph22->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph22->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph22->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle23Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph23->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph23->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph23->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph23->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph23->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle24Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph24->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph24->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph24->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph24->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph24->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle25Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph25->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph25->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph25->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph25->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph25->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle26Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph26->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph26->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph26->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph26->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph26->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle27Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph27->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph27->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph27->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph27->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph27->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle28Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph28->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph28->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph28->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph28->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph28->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle29Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph29->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph29->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph29->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph29->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph29->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle3Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph3->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph3->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph3->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph3->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph3->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle30Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph30->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph30->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph30->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph30->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph30->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle31Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph31->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph31->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph31->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph31->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph31->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle32Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph32->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph32->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph32->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph32->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph32->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle33Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph33->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph33->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph33->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph33->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph33->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle34Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph34->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph34->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph34->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph34->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph34->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle35Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph35->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph35->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph35->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph35->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph35->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle36Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph36->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph36->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph36->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph36->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph36->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle37Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph37->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph37->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph37->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph37->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph37->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle38Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph38->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph38->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph38->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph38->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph38->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle39Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph39->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph39->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph39->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph39->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph39->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle4Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph4->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph4->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph4->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph4->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph4->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle40Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph40->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph40->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph40->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph40->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph40->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle41Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph41->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph41->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph41->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph41->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph41->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle42Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph42->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph42->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph42->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph42->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph42->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle43Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph43->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph43->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph43->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph43->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph43->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle44Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph44->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph44->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph44->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph44->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph44->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle45Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph45->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph45->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph45->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph45->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph45->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle46Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph46->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph46->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph46->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph46->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph46->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle47Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph47->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph47->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph47->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph47->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph47->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle48Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph48->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph48->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph48->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph48->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph48->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle49Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph49->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph49->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph49->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph49->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph49->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle5Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph5->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph5->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph5->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph5->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph5->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle50Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph50->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph50->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph50->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph50->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph50->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle51Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph51->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph51->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph51->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph51->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph51->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle52Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph52->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph52->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph52->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph52->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph52->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle53Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph53->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph53->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph53->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph53->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph53->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle54Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph54->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph54->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph54->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph54->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph54->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle55Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph55->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph55->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph55->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph55->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph55->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle56Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph56->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph56->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph56->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph56->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph56->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle57Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph57->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph57->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph57->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph57->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph57->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle58Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph58->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph58->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph58->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph58->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph58->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle59Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph59->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph59->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph59->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph59->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph59->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle6Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph6->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph6->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph6->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph6->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph6->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle60Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph60->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph60->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph60->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph60->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph60->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle61Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph61->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph61->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph61->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph61->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph61->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle62Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph62->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph62->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph62->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph62->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph62->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle63Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph63->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph63->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph63->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph63->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph63->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle64Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph64->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph64->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph64->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph64->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph64->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle65Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph65->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph65->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph65->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph65->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph65->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle66Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph66->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph66->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph66->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph66->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph66->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle67Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph67->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph67->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph67->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph67->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph67->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle68Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph68->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph68->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph68->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph68->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph68->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle69Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph69->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph69->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph69->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph69->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph69->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle7Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph7->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph7->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph7->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph7->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph7->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle70Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph70->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph70->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph70->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph70->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph70->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle71Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph71->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph71->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph71->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph71->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph71->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle72Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph72->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph72->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph72->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph72->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph72->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle73Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph73->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph73->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph73->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph73->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph73->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle74Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph74->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph74->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph74->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph74->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph74->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle75Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph75->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph75->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph75->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph75->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph75->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle76Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph76->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph76->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph76->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph76->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph76->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle77Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph77->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph77->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph77->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph77->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph77->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle78Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph78->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph78->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph78->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph78->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph78->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle79Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph79->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph79->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph79->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph79->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph79->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle8Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph8->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph8->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph8->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph8->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph8->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle80Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph80->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph80->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph80->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph80->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph80->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle81Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph81->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph81->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph81->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph81->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph81->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle9Click(TObject *Sender)
{
if (coBlack->Tag==1) {
   Glyph9->ImageIndex=1;
}
else if (coOrange->Tag==1) {
   Glyph9->ImageIndex=2;
}
else if (coBrown->Tag==1) {
   Glyph9->ImageIndex=3;
}
else if (coYellow->Tag==1) {
   Glyph9->ImageIndex=4;
}
else if (coPink->Tag==1) {
   Glyph9->ImageIndex=5;
}
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle1DblClick(TObject *Sender)
{
Glyph1->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle10DblClick(TObject *Sender)
{
  Glyph10->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle11DblClick(TObject *Sender)
{
	Glyph11->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle12DblClick(TObject *Sender)
{
	  Glyph12->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle13DblClick(TObject *Sender)
{
	Glyph13->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle14DblClick(TObject *Sender)
{
	  Glyph14->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle15DblClick(TObject *Sender)
{
	Glyph15->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle16DblClick(TObject *Sender)
{
  Glyph16->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle17DblClick(TObject *Sender)
{
 Glyph17->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle18DblClick(TObject *Sender)
{
   Glyph18->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle19DblClick(TObject *Sender)
{
	  Glyph19->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle2DblClick(TObject *Sender)
{
	 Glyph2->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle20DblClick(TObject *Sender)
{
	  Glyph20->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle21DblClick(TObject *Sender)
{
	  Glyph21->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle22DblClick(TObject *Sender)
{
   Glyph22->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle23DblClick(TObject *Sender)
{
	Glyph23->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle24DblClick(TObject *Sender)
{
	   Glyph24->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle25DblClick(TObject *Sender)
{
	  Glyph25->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle26DblClick(TObject *Sender)
{
   Glyph26->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle27DblClick(TObject *Sender)
{
   Glyph27->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle28DblClick(TObject *Sender)
{
	   Glyph28->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle29DblClick(TObject *Sender)
{
	Glyph29->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle3DblClick(TObject *Sender)
{
	Glyph3->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle30DblClick(TObject *Sender)
{
   Glyph30->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle31DblClick(TObject *Sender)
{
 Glyph31->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle32DblClick(TObject *Sender)
{
  Glyph32->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle33DblClick(TObject *Sender)
{
	Glyph33->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle34DblClick(TObject *Sender)
{
   Glyph34->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle35DblClick(TObject *Sender)
{
	 Glyph35->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle36DblClick(TObject *Sender)
{
	 Glyph36->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle37DblClick(TObject *Sender)
{
   Glyph37->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle38DblClick(TObject *Sender)
{
  Glyph38->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle39DblClick(TObject *Sender)
{
	Glyph39->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle4DblClick(TObject *Sender)
{
	 Glyph4->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle40DblClick(TObject *Sender)
{
 Glyph40->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle41DblClick(TObject *Sender)
{
  Glyph41->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle42DblClick(TObject *Sender)
{
		 Glyph42->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle43DblClick(TObject *Sender)
{
	Glyph43->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle44DblClick(TObject *Sender)
{
	Glyph44->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle45DblClick(TObject *Sender)
{
	Glyph45->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle46DblClick(TObject *Sender)
{
   Glyph46->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle47DblClick(TObject *Sender)
{
   Glyph47->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle48DblClick(TObject *Sender)
{
   Glyph48->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle49DblClick(TObject *Sender)
{
   Glyph49->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle5DblClick(TObject *Sender)
{
   Glyph5->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle50DblClick(TObject *Sender)
{
  Glyph50->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle51DblClick(TObject *Sender)
{
   Glyph51->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle52DblClick(TObject *Sender)
{
  Glyph52->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle53DblClick(TObject *Sender)
{
  Glyph53->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle54DblClick(TObject *Sender)
{
  Glyph54->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle55DblClick(TObject *Sender)
{
  Glyph55->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle56DblClick(TObject *Sender)
{
  Glyph56->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle57DblClick(TObject *Sender)
{
 Glyph57->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle58DblClick(TObject *Sender)
{
  Glyph58->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle59DblClick(TObject *Sender)
{
  Glyph59->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle6DblClick(TObject *Sender)
{
  Glyph6->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle60DblClick(TObject *Sender)
{
	Glyph60->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle61DblClick(TObject *Sender)
{
   Glyph61->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle62DblClick(TObject *Sender)
{
   Glyph62->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle63DblClick(TObject *Sender)
{
   Glyph63->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle64DblClick(TObject *Sender)
{
 Glyph64->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle65DblClick(TObject *Sender)
{
  Glyph65->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle66DblClick(TObject *Sender)
{
Glyph66->ImageIndex=6;

}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle67DblClick(TObject *Sender)
{
	   Glyph67->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle68DblClick(TObject *Sender)
{
  Glyph68->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle69DblClick(TObject *Sender)
{
  Glyph69->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle7DblClick(TObject *Sender)
{
  Glyph7->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle70DblClick(TObject *Sender)
{
   Glyph70->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle71DblClick(TObject *Sender)
{
 Glyph71->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle72DblClick(TObject *Sender)
{
  Glyph72->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle73DblClick(TObject *Sender)
{
  Glyph73->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle74DblClick(TObject *Sender)
{
   Glyph74->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle75DblClick(TObject *Sender)
{
  Glyph75->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle76DblClick(TObject *Sender)
{
  Glyph76->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle77DblClick(TObject *Sender)
{
   Glyph77->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle78DblClick(TObject *Sender)
{
 Glyph78->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle79DblClick(TObject *Sender)
{
  Glyph79->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle8DblClick(TObject *Sender)
{
  Glyph8->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle80DblClick(TObject *Sender)
{
  Glyph80->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle81DblClick(TObject *Sender)
{
  Glyph81->ImageIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall Ttm::Rectangle9DblClick(TObject *Sender)
{
  Glyph9->ImageIndex=6;
}
//---------------------------------------------------------------------------


