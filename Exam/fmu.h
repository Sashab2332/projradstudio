//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <System.ImageList.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *Main;
	TTabItem *Game1;
	TButton *buGame;
	TButton *buPref;
	TTabItem *Pref;
	TTabItem *Game2;
	TImageList *ImageList1;
	TGridPanelLayout *GridPanelLayout1;
	TRectangle *Rectangle1;
	TGlyph *Glyph1;
	TRectangle *Rectangle2;
	TGlyph *Glyph2;
	TRectangle *Rectangle3;
	TGlyph *Glyph3;
	TRectangle *Rectangle4;
	TGlyph *Glyph4;
	TRectangle *Rectangle5;
	TGlyph *Glyph5;
	TRectangle *Rectangle6;
	TGlyph *Glyph6;
	TRectangle *Rectangle7;
	TGlyph *Glyph7;
	TRectangle *Rectangle8;
	TGlyph *Glyph8;
	TRectangle *Rectangle9;
	TGlyph *Glyph9;
	TRectangle *Rectangle10;
	TGlyph *Glyph10;
	TRectangle *Rectangle11;
	TGlyph *Glyph11;
	TRectangle *Rectangle12;
	TGlyph *Glyph12;
	TRectangle *Rectangle13;
	TGlyph *Glyph13;
	TRectangle *Rectangle14;
	TGlyph *Glyph14;
	TRectangle *Rectangle15;
	TGlyph *Glyph15;
	TRectangle *Rectangle16;
	TGlyph *Glyph16;
	TRectangle *Rectangle17;
	TGlyph *Glyph17;
	TRectangle *Rectangle18;
	TGlyph *Glyph18;
	TRectangle *Rectangle19;
	TGlyph *Glyph19;
	TRectangle *Rectangle20;
	TGlyph *Glyph20;
	TRectangle *Rectangle21;
	TGlyph *Glyph21;
	TRectangle *Rectangle22;
	TGlyph *Glyph22;
	TRectangle *Rectangle23;
	TGlyph *Glyph23;
	TRectangle *Rectangle24;
	TGlyph *Glyph24;
	TRectangle *Rectangle25;
	TGlyph *Glyph25;
	TTimer *Timer1;
	TLabel *laTime;
	TButton *BuGame2;
	TButton *buBack1;
	TButton *buBack2;
	TButton *buBack3;
	TCheckBox *Check;
	TLabel *laTime1;
	TTimer *Timer2;
	TImage *im;
	TImage *im1;
	TLabel *lala;
	TImage *Image1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image5;
	TImage *Image6;
	TImage *Image7;
	TImage *Image8;
	TImage *Image9;
	TGridPanelLayout *GridPanelLayout2;
	TRectangle *Rectangle26;
	TGlyph *Glyph26;
	TRectangle *Rectangle27;
	TGlyph *Glyph27;
	TRectangle *Rectangle28;
	TGlyph *Glyph28;
	TRectangle *Rectangle29;
	TGlyph *Glyph29;
	TRectangle *Rectangle30;
	TGlyph *Glyph30;
	TRectangle *Rectangle31;
	TGlyph *Glyph31;
	TRectangle *Rectangle32;
	TGlyph *Glyph32;
	TRectangle *Rectangle33;
	TGlyph *Glyph33;
	TRectangle *Rectangle34;
	TGlyph *Glyph34;
	TRectangle *Rectangle35;
	TGlyph *Glyph35;
	TRectangle *Rectangle36;
	TGlyph *Glyph36;
	TRectangle *Rectangle37;
	TGlyph *Glyph37;
	TRectangle *Rectangle38;
	TGlyph *Glyph38;
	TRectangle *Rectangle39;
	TGlyph *Glyph39;
	TRectangle *Rectangle40;
	TGlyph *Glyph40;
	TRectangle *Rectangle41;
	TGlyph *Glyph41;
	TRectangle *Rectangle42;
	TGlyph *Glyph42;
	TRectangle *Rectangle43;
	TGlyph *Glyph43;
	TRectangle *Rectangle44;
	TGlyph *Glyph44;
	TRectangle *Rectangle45;
	TGlyph *Glyph45;
	TRectangle *Rectangle46;
	TGlyph *Glyph46;
	TRectangle *Rectangle47;
	TGlyph *Glyph47;
	TRectangle *Rectangle48;
	TGlyph *Glyph48;
	TRectangle *Rectangle49;
	TGlyph *Glyph49;
	TRectangle *Rectangle50;
	TGlyph *Glyph50;
	TRectangle *Rectangle51;
	TGlyph *Glyph51;
	TRectangle *Rectangle52;
	TGlyph *Glyph52;
	TRectangle *Rectangle53;
	TGlyph *Glyph53;
	TRectangle *Rectangle54;
	TGlyph *Glyph54;
	TRectangle *Rectangle55;
	TGlyph *Glyph55;
	TRectangle *Rectangle56;
	TGlyph *Glyph56;
	TRectangle *Rectangle57;
	TGlyph *Glyph57;
	TRectangle *Rectangle58;
	TGlyph *Glyph58;
	TRectangle *Rectangle59;
	TGlyph *Glyph59;
	TRectangle *Rectangle60;
	TGlyph *Glyph60;

	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Rectangle1Click(TObject *Sender);
	void __fastcall buGameClick(TObject *Sender);
	void __fastcall buBack1Click(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);

	void __fastcall BuGame2Click(TObject *Sender);
	void __fastcall Rectangle26Click(TObject *Sender);
	void __fastcall Timer2Timer(TObject *Sender);
	void __fastcall buPrefClick(TObject *Sender);
private:	// User declarations

public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
	TRectangle *FListRectangle[60];
	double FTimeValue;


};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif

