//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Controls.Presentation.hpp>

//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *TabControl1;
	TTabItem *tiMenu;
	TTabItem *tiLevel;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *tiPlay;
	TTabItem *TabItem6;
	TLayout *lyTop;
	TButton *buReset;
	TLabel *laQuestion;
	TLabel *laTime;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buAnswer1;
	TButton *buAnswer2;
	TButton *buAnswer3;
	TButton *buAnswer4;
	TButton *buAnswer5;
	TButton *buAnswer6;
	TLayout *Layout1;
	TGridPanelLayout *GridPanelLayout2;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TRectangle *Rectangle3;
	TRectangle *Rectangle4;
	TRectangle *Rectangle5;
	TRectangle *Rectangle6;
	TRectangle *Rectangle7;
	TRectangle *Rectangle8;
	TRectangle *Rectangle9;
	TRectangle *Rectangle10;
	TRectangle *Rectangle11;
	TRectangle *Rectangle12;
	TRectangle *Rectangle13;
	TRectangle *Rectangle14;
	TRectangle *Rectangle15;
	TRectangle *Rectangle16;
	TRectangle *Rectangle17;
	TRectangle *Rectangle18;
	TRectangle *Rectangle19;
	TRectangle *Rectangle20;
	TRectangle *Rectangle21;
	TRectangle *Rectangle22;
	TRectangle *Rectangle23;
	TRectangle *Rectangle24;
	TRectangle *Rectangle25;
	TTimer *tmPlay;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall buAnswer1Click(TObject *Sender);
	void __fastcall tmPlayTimer(TObject *Sender);
private:	// User declarations
int FCuntCorrect;
int* RandomArrayUnique(intaMax, int aCount);
int FCountWrong;
int FNumberCorrect;
double FTimeValue;
TList *FListBox;
TList *FListAnswer;
void DoReset();
void DoContinue();
void DoAnswer(int aVAlue);
void DoFinish();
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
const int cMaxBox=25;
const int cMaxAnswer=6;
const int cMinPossible=4;
const int cMaxPossible=14;
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
