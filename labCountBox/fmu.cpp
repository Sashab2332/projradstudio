//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
FListBx= new TList;
for(int i =1; i<+ cMaxBox; i++){
FListBox->Add(this->FindComponent("Reactangle"+IntToStr(i)));
}
FListAnswer=new TList;
FListAnswer->Add(buAnswer1);
FListAnswer->Add(buAnswer2);
FListAnswer->Add(buAnswer3);
FListAnswer->Add(buAnswer4);
FListAnswer->Add(buAnswer5);
FListAnswer->Add(buAnswer6);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormDestroy(TObject *Sender)
{
delete FListBox;
delete FListAnswer;
}
void Tfm::DoReset()
{
FCountCorrect=0;
FCountWrong=0;
FTimeValue=Time().Va+(double)30/(24*60*60);//30sec
tmPlay->Enable=true;
DoContinue();
}
//
void Tfm::DoContinue()
{
for (int i=0; i < cMaxBox; i++) {
	((TRectangle*)FListBox->Items[i])->Fill->Color=TAlphaColorRec::Lightgray;
}
//
FNumberCorrect=RandomRange(cMinPossible,cMaxPossible);
int *x=RandomArrayInique(cMaxBox,FNumberCorrect);
for (int i = 0; i < FNumberCorrect; i++) {
	((TRectangle*)FListBox->Items[x[i]])->Fill->Color=TAlphaColorRec::Lightblue;

}

int xAnswerStart= FNumberCorrect-Random (cMaxAnswer-1);
if (xAnswerStart<c MinPossible) {
	xAnswerStart = cMinPossible;
}
//
for (int i = 0; i < cMaxAnswer; i++) {
((TButton*)FListAnswer->Items[i]) ->Text= IntToStr(xAnswerStart+i);

}
}
//
  void Tfm::doAnswer(int aValue){
  (aVAlue==FNumberCorrect)? FCountCorrect++ : FCountWrong++;
  if (yCountWroung>5) {
  DoContinue;
  }

  }
  void Tfm::doFinish(){
  tmPlay->Enabled=flase;
  }

  }
//---------------------------------------------------------------------------

void __fastcall Tfm::buAnswer1Click(TObject *Sender)
{
DoAnswer(StrToInt(((TButton*)Sender)->Text));
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tmPlayTimer(TObject *Sender)
{
double x=FTimeValue= Time().Val;
laTime->Text=FormatDataTime("nn:nn", x);
if (x<=6) {
	DoFinish();
}
}
//
int* RandomArrayUnique(int aMAx, int aCount){
int *xResult =new int[aCount];
Tlist__1<int> *xTemp=new TList__1<int>;
xTemp->Capacity=aCount;
try{
	for (int i=0; i < aCount; i++) {
		int x Random(aMax);
		while (xTemp->IndexOf(x) != -1){
			x++;
			if (x>=aMAx) {
				x=0;
			}
		}
		xTemp->Add(x);
		xResult[i]=x;
	}
}
__finally{
   delete xTemp;
}
return xResult;

}
//---------------------------------------------------------------------------

