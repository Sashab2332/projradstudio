//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
   for (int i = 0; i < 359; i++) {
	   FListRectangle[i]=
		  (TRectangle*)(this->FindComponent("Rectangle" + IntToStr(i+1)));
   }
   for (int i = 0; i < 359; i++) {
	   FListGlyph[i]=
		  (TGlyph*)(this->FindComponent("Glyph" + IntToStr(i+1)));
   }
   for (int j = 0; j < 56; j++) {

		 FListBomb[j]=Random(360);
		 for (int i = 0; i < 359; i++) {
			 if (FListBomb[j]==i) {
				   FListRectangle[i]->Tag=1;
			 }
		 }
   }
}
//---------------------------------------------------------------------------


void __fastcall Tfm::Rectangle1Click(TObject *Sender)
{
for (int i = 0; i < 359; i++) {

	 if (FListRectangle[i]->Name==((TRectangle*) Sender)->Name) {

//----------------------------------------------------------
		 if (i==0&&FListRectangle[i]->Tag==1) {
			FListGlyph[i]->ImageIndex=11;
		   //	break;
		 }
		 if (i==0&&FListRectangle[i]->Tag!=1) {
			if (FListRectangle[i+1]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+20]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+21]->Tag==1) {
				Count++;
			}
			FListGlyph[i]->ImageIndex=Count;
			Count=0;
		   //	break;
		 }
// //------------------------------------
		 if (i==340&&FListRectangle[i]->Tag==1) {
			FListGlyph[i]->ImageIndex=11;
			//break;
		 }
		 if (i==340&&FListRectangle[i]->Tag!=1) {
			if (FListRectangle[i-20]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i-19]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+1]->Tag==1) {
				Count++;
			}
			FListGlyph[i]->ImageIndex=Count;
			Count=0;
			//break;
		 }
////---------------------
		 if (i==19&&FListRectangle[i]->Tag==1) {
			FListGlyph[i]->ImageIndex=11;
		   //	break;
		 }
		 if (i==19&&FListRectangle[i]->Tag!=1) {
			if (FListRectangle[i-1]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+19]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+20]->Tag==1) {
				Count++;
			}
			FListGlyph[i]->ImageIndex=Count;
			Count=0;
		   //	break;
		 }
		 //--------------------
		 if (i==359&&FListRectangle[i]->Tag==1) {
			FListGlyph[i]->ImageIndex=11;
		  // break;
		 }
		 if (i==359&&FListRectangle[i]->Tag!=1) {
			if (FListRectangle[i-1]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i-20]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i-21]->Tag==1) {
				Count++;
			}
			FListGlyph[i]->ImageIndex=Count;
			Count=0;
			  break;
		 }

		 if (i!=0&&i!=340&&i%20==0&&FListRectangle[i]->Tag==1) {
			FListGlyph[i]->ImageIndex=11;
		   break;
		 }
		 if (i!=0&&i!=340&&i%20==0&&FListRectangle[i]->Tag!=1) {
			if (FListRectangle[-20]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i-19]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+1]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+20]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+21]->Tag==1) {
				Count++;
			}
			FListGlyph[i]->ImageIndex=Count;
			Count=0;
		   //	break;
		 }

		 if (i>0&&i<19&&FListRectangle[i]->Tag==1) {
			FListGlyph[i]->ImageIndex=11;

		 }
		 if (i>0&&i<19&&FListRectangle[i]->Tag!=1) {
			if (FListRectangle[-1]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+1]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+19]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+20]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+21]->Tag==1) {
				Count++;
			}
			FListGlyph[i]->ImageIndex=Count;
			Count=0;
		   //	break;
		 }

			 if (i>340&&i<359&&FListRectangle[i]->Tag==1) {
			FListGlyph[i]->ImageIndex=11;

		 }
		 if (i>340&&i<360&&FListRectangle[i]->Tag!=1) {
			if (FListRectangle[-1]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i-21]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i-20]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i-19]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+1]->Tag==1) {
				Count++;
			}
			FListGlyph[i]->ImageIndex=Count;
			Count=0;

		 }

			 if (i!=19&&i!=359&&(i+1)%20==0&&FListRectangle[i]->Tag==1) {
			FListGlyph[i]->ImageIndex=11;
			 break;
		 }
		 if (i!=19&&i!=359&&(i+1)%20==0&&FListRectangle[i]->Tag!=1) {
			if (FListRectangle[-1]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i-19]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i-20]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+19]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+20]->Tag==1) {
				Count++;
			}
			FListGlyph[i]->ImageIndex=Count;
			Count=0;

		 }

		 if ((i+1)%20!=0&&
			  i<340&&
			  i>19&&
			  i%20!=0&&
			  FListRectangle[i]->Tag==1) {
			FListGlyph[i]->ImageIndex=11;
			 break;
		 }
		 if ((i+1)%20!=0&&
			  i<340&&
			  i>19&&
			  i%20!=0&&FListRectangle[i]->Tag!=1) {
			if (FListRectangle[i-21]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i-20]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i-19]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i-1]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+1]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+19]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+20]->Tag==1) {
				Count++;
			}
			if (FListRectangle[i+21]->Tag==1) {
				Count++;
			}
			FListGlyph[i]->ImageIndex=Count;
			Count=0;

		 }
	 }
}
//laTimer->Text =((TRectangle*) Sender)->Tag;
//laTimer->Text = FListRectangle[5]->Name;
}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------


void __fastcall Tfm::Rectangle1DblClick(TObject *Sender)
{
//for (int i = 0; i < 359; i++) {
//
//	 if (FListRectangle[i]->Name==((TRectangle*) Sender)->Name)  {
//	 FListGlyph[i]->ImageIndex=10;
//	 }
//}
//---------------------------------------------------------------------------
}


//---------------------------------------------------------------------------

//void __fastcall Rectangle1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
//		  float X, float Y)
//{
//if (Button == mbRight){
// for (int i = 0; i < 359; i++) {
//
//	 if (FListRectangle[i]->Name==((TRectangle*) Sender)->Name)  {
//	 FListGlyph[i]->ImageIndex=10;
//	 }
//}
//}
//---------------------------------------------------------------------------


